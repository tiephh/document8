package com.example.document8;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {
    private List<Note> mData;
    private LayoutInflater mInflater;
    public ItemClickListener mClickListener;
    Context context;

    MyRecyclerViewAdapter(Context context, List<Note> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.col_item_recycle, parent, false);
        return new ViewHolder(view);
    }

    @NonNull
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Note note = mData.get(position);
        holder.bind(note);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_title;
        TextView tv_content;
        TextView tv_time;
        CheckBox cb_check;
        LinearLayout layout_content;

        ViewHolder(View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_content = itemView.findViewById(R.id.tv_content);
            tv_time = itemView.findViewById(R.id.tv_time);
            cb_check = itemView.findViewById(R.id.cb_check);
            layout_content = itemView.findViewById(R.id.layout_content);
        }

        public void bind(Note note) {
            String content = note.getContent();
//            if (content.length() > 20) {
//                tv_content.setText(content.substring(0, 25) + "....");
//            }
            tv_content.setText(content);
            tv_title.setText(note.getTiltle());
            tv_time.setText(note.getTime());
            if (note.isShow()) {
                cb_check.setVisibility(View.VISIBLE);
            } else {
                cb_check.setVisibility(View.GONE);
            }
            if(note.isCheck() == false){
                cb_check.setChecked(false);
            }
            layout_content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mClickListener != null)
                        mClickListener.onItemClick(note, getAdapterPosition());
                }
            });
            layout_content.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    //note.setShow(true);
                    //notifyDataSetChanged();
                    if (mClickListener != null) {
                        mClickListener.onItemLongClick(note, getAdapterPosition());
                    }
                    return true;
                }
            });
            cb_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (mClickListener != null) {
                        mClickListener.checkBoxClick(getAdapterPosition(), isChecked);
                    }
                }
            });
        }
    }

    // convenience method for getting data at click position
    Note getItem(int id) {
        return mData.get(id);
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(Note note, int position);
        void checkBoxClick(int position, boolean checked);
        void onItemLongClick(Note note, int position);
    }
}

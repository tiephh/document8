package com.example.document8;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {
    TextView tv_title;
    EditText et_content;
    Button btn_back;
    ImageButton btn_edit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        tv_title = findViewById(R.id.tv_title);
        btn_edit = findViewById(R.id.btn_edit);
        et_content = findViewById(R.id.et_content);
        btn_back = findViewById(R.id.btn_back);
        Note data = (Note) getIntent().getSerializableExtra("data");
        int idBack = data.getId();
        et_content.setText(data.getContent());
        String checkEdit = et_content.getText().toString();
        tv_title.setText(data.getTiltle());


        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!checkEdit.equals(et_content.getText().toString())){
                    AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity2.this);
                    dialog.setTitle( "Do you want to save" );
                    dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialoginterface, int i) {
                            dialoginterface.cancel();
                        }});
                    dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialoginterface, int i) {
                            sendData(et_content.getText().toString(), idBack);
                        }
                    });
                    dialog.show();
                }else {
                    finish();
                }
            }
        });
        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendData(et_content.getText().toString(), idBack);
            }
        });
    }
    private  void sendData(String data, int id){
        Intent intent = new Intent();
        intent.putExtra("dataBack", data);
        intent.putExtra("idBack", id);
        setResult(RESULT_OK, intent);
        finish();
    }
}
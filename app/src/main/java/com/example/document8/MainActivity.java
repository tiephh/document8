package com.example.document8;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    DataBase database;
    RecyclerView rv_content;
    MyRecyclerViewAdapter adapter;
    ArrayList<Note> arrayList;
    int positionCheck;
    TextView tv_check;
    private static final int SECOND_ACTIVITY_REQUEST_CODE = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        database = new DataBase(this, "note.sqlite", null, 1);
        database.QueryData("CREATE TABLE IF NOT EXISTS note(ID INTEGER PRIMARY KEY AUTOINCREMENT, title NVARCHAR(20), content NVARCHAR(500), time NVARCHAR(40))");
        initView();
        selectDb();
        checkList();
    }
    private  void checkList(){
        if(arrayList.size() == 0){
            tv_check.setVisibility(View.VISIBLE);
        }else {
            tv_check.setVisibility(View.GONE);
        }
    }
    private void initView(){
        tv_check = findViewById(R.id.tv_check);
        rv_content = findViewById(R.id.rv_content);
        rv_content.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_content.setLayoutManager(linearLayoutManager);
        arrayList = new ArrayList<>();
        adapter = new MyRecyclerViewAdapter(this, arrayList);
        adapter.setClickListener(new MyRecyclerViewAdapter.ItemClickListener() {
            @Override
            public void onItemClick(Note note, int position) {
                Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                intent.putExtra("data", note);
                startActivityForResult(intent, SECOND_ACTIVITY_REQUEST_CODE);
            }

            @Override
            public void checkBoxClick(int position, boolean checked) {
                arrayList.get(position).setCheck(checked);
                positionCheck = position;
            }

            @Override
            public void onItemLongClick(Note note, int position) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
                dialog.setTitle( "Do you want delete" );
                dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.cancel();
                    }});
                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        int idDele = arrayList.get(position).getId();
                        database.QueryData("DELETE FROM note WHERE ID = '"+idDele+"'");
                        selectDb();
                        checkList();
                    }
                });
                dialog.show();
            }


        });
        rv_content.setAdapter(adapter);
    }

    private void selectDb(){
        Cursor dataInf = database.GetData("SELECT * FROM note");
        arrayList.clear();

        while (dataInf.moveToNext()){
            int id = dataInf.getInt(0);
            String title = dataInf.getString(1);
            String content = dataInf.getString(2);
            String time = dataInf.getString(3);
            arrayList.add(new Note(id, title, content, time, false, false));
        }
        Collections.reverse(arrayList);
        adapter.notifyDataSetChanged();
    }
    @Override

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_items, menu);
        return true;
    }


    private void DialogaAdd(){
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add);
        EditText tv_tilte_dialog = dialog.findViewById(R.id.tv_title_dialog);
        EditText tv_content_dialog = dialog.findViewById(R.id.tv_content_dialog);
        Button btn_add_dialog = dialog.findViewById(R.id.btn_add_dialog);
        Button btn_cancle_dialog = dialog.findViewById(R.id.btn_cancle_dialog);
        DateFormat df = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
        String date = df.format(Calendar.getInstance().getTime());
        btn_add_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database.QueryData("INSERT INTO note VALUES (null, '"+tv_tilte_dialog.getText()+"', '"+tv_content_dialog.getText()+"', '"+date+"')");
                dialog.dismiss();
                selectDb();
                checkList();
                Toast.makeText(MainActivity.this, "Success", Toast.LENGTH_SHORT).show();
            }
        });
        btn_cancle_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog(tv_tilte_dialog.getText().toString(), tv_content_dialog.getText().toString(), date);
                dialog.dismiss();
                selectDb();
            }
        });
        dialog.show();
    }

    private void alertDialog(String title, String content, String date) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle( "Do you want to save" );
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                dialoginterface.cancel();
            }});
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                database.QueryData("INSERT INTO note VALUES (null, '"+title+"', '"+content+"', '"+date+"')");
                selectDb();
                checkList();
            }
        });
        dialog.show();
    }


    private void alertDialogDelete() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle( "Do you want to delete" );
        dialog.setIcon(R.drawable.ic_delete_icon);
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                 public void onClick(DialogInterface dialoginterface, int i) {
                    dialoginterface.cancel();
                    arrayList.get(positionCheck).setShow(false);
                     arrayList.get(positionCheck).setCheck(false);
                    adapter.notifyDataSetChanged();
                 }});
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        for(int j = 0; j< arrayList.size(); j++){
                            if(arrayList.get(j).isCheck()){
                                int idMultiple = arrayList.get(j).getId();
                                database.QueryData("DELETE FROM note WHERE ID = '"+idMultiple+"'");
                            }
                        }
                        selectDb();
                        checkList();
                    }
                });
        dialog.show();
    }
    @Override


    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.item_add:
                DialogaAdd();
                selectDb();
                return true;
            case R.id.item_choose_multi:
                for(int i = 0; i < arrayList.size(); i++){
                    arrayList.get(i).setShow(true);
                }
                adapter.notifyDataSetChanged();
                return true;
            case R.id.item_delete:
                int count = 0;
                for(int j = 0; j< arrayList.size(); j++){
                    if(arrayList.get(j).isCheck() && arrayList.get(j).isShow()){
                        count ++;
                    }
                }
                if(count >0) {
                    alertDialogDelete();
                    count = 0;
                }else
                    Toast.makeText(MainActivity.this, "Ban chua chon", Toast.LENGTH_SHORT).show();
                return true;
            default:
                break;
        }

        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SECOND_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String dataBack = data.getStringExtra("dataBack");
                int idBack = data.getIntExtra("idBack", 0);
                database.QueryData("UPDATE note SET content = '"+dataBack+"' WHERE ID = '"+idBack+"'");
                selectDb();
            }
        }
    }
}
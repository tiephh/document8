package com.example.document8;

import java.io.Serializable;

public class Note implements Serializable {
    private int id;
    private String tiltle;
    private String content;
    private String time;
    private boolean isShow;
    private boolean isCheck;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTiltle() {
        return tiltle;
    }

    public void setTiltle(String tiltle) {
        this.tiltle = tiltle;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isShow() {
        return isShow;
    }

    public void setShow(boolean show) {
        isShow = show;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public Note(int id, String tiltle, String content, String time, boolean isShow, boolean isCheck) {
        this.id = id;
        this.tiltle = tiltle;
        this.content = content;
        this.time = time;
        this.isShow = isShow;
        this.isCheck = isCheck;
    }
}
